\section{Testing Strategy}
\label{sec:approach}

\autoref{fig:approach} outlines our testing method to test an obfuscator $O$.
First, we generate a program $P$ using a random program generator.  Then, we
carry this program along two paths.  In one path, we compile $P$ to get an
executable $C(P)$.  In the second path, we first obfuscate $P$ using the
obfuscator $O$ to get an obfuscated program, $O(P)$.  We compile $O(P)$ to get a
different executable $C(O(P))$.  We run each executable and compare the outputs.
If the outputs do not match, we say that the obfuscator $O$ failed that test.
Otherwise, the obfuscator passed.  We repeat this test millions of times for
each obfuscator using different randomly generated programs. 

\begin{figure*}[t]                                                                   
  \centering                                                                        
  \includegraphics[width=0.92\textwidth]{figures/approach.png}  \\          
  \caption{This is an outline of our testing approach.  The main goal of this is
to compare a program and its obfuscated version using the behavior of their
executables.  The executables should have identical output behavior.} 
  \label{fig:approach}                                                        
\end{figure*} 

\boldpara{Random Program Generator:}  Central to our approach is the random
program generator, a program which generates programs in a given programming
langauge randomly.  Ideally, the random program would be selected uniformly at
random a program from all possible programs in a given programming language.
However, a pseudorandom program generator should suffice.  The goal is to
generate a large, diverse set of test inputs for the obfuscator.

\boldpara{Programs:} In our approach, we approximate the semantics of a program
using their input-ouput behavior.  Two programs are equivalent if their
input-output behavior is identical.  We restricted our input programs to those
that have some observable output. 

\boldpara{Obfuscators and Compilers:}
An obfuscator is a compiler with an additional security requirement.  Put
simply, given a program $P$, a compiler $C$ produces an executable program
$C(P)$.  The compiled program $C(P)$ must be 
\begin{inparaenum}[1)]
\item semantically-equivalent to the original program $P$, and
\item it must not be much slower than the executable program one could write
by hand.
\end{inparaenum}
Given an input program $P$, an obfuscator $O$ compiles $P$ into $O(P)$.  The
obfuscator meets the security requirement if $O(P)$ is more difficult to read
and understand.  In our study, we focused on obfuscators that output programs in
the same language as the input program.   

\boldpara{Oracle-Testing:}
How do we know that the obfuscated program $O(P)$ is correct?  Technically, it
is correct if it is equivalent to $P$.  However, deciding  whether two programs
are equivalent is an undecidable problem.  In our approach, we resort to
oracle-testing.  We compare $P$ and $O(P)$ by comparing the input-output
behavior of their executables, $C(P)$ and $C(O(P))$.  The compiler is the
oracle. In other words,  we assume that the compiler is correct in that it
compiler does not change the input-output behavior of its input programs $P$ and
$O(P)$.  (This is a very simplistic assumption.  Compilers have bugs
too.~\cite{}) If the executables $C(P)$ and $C(O(P))$ differ in their
input-output behavior, we'll assume that this is because $O(P)$ is incorrect,
and proceed to search for possible bugs in $O$.


